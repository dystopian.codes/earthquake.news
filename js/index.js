var scene, renderer,camera, wireframeGeometry,maxAnisotropy, tile, rotation;



////////////////////////////////////////////////////////////////////////////////////
// STARTING THE APPLICATION
////////////////////////////////////////////////////////////////////////////////////

var $place, $magnitude;

$( document ).ready(function() {

    $("#next-bt").click(function(){
        nextTile();   
    });

    $("#prev-bt").click(function(){
       prevTile();
    });
    $place = $('.place');
    $magnitude = $('.magnitude');
    //getting the data from the api
    getData();

});



////////////////////////////////////////////////////////////////////////////////////
// RETRIEVING DATA FROM THE API
////////////////////////////////////////////////////////////////////////////////////

var data;
var count =0 ;
var index = 0;

function getData(){
    var jsonDataUrl = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_week.geojson";
    $.getJSON( jsonDataUrl, function(response) {       
        data = response.features;    
        count = data.length;     
        //on success initialize
        init();
    })
    .fail(function() {
        console.log( "error" );
    });    
}


////////////////////////////////////////////////////////////////////////////////////
// INITIALIZING SCENE
////////////////////////////////////////////////////////////////////////////////////

function init() {
    //creating three.js scene
    scene = new THREE.Scene();
    var W = window.innerWidth;
    var H = window.innerHeight;
    renderer = new THREE.WebGLRenderer();
    maxAnisotropy = renderer.getMaxAnisotropy();
    //background color
    renderer.setClearColor(0x202020);
    renderer.setSize(W, H);
    //adding camera and light
    camera = new THREE.PerspectiveCamera(45, W / H, 0.01, 200);
    camera.position.set(50, 30, 1);
    camera.lookAt(scene.position);
    var light = new THREE.HemisphereLight(0xffffff, 0xffffff, 1);
    light.position.set(0, 50, 0);
    scene.add(light) ;
    document.body.appendChild(renderer.domElement);
    //getting current camera rotation, used later in the sliding animation 
    rotation = camera.rotation.y;
    //this controller is used to avoid zooming on mobile devices
    var controls = new THREE.OrbitControls( camera, renderer.domElement );
    controls.minDistance = 58.3;
    controls.maxDistance = 58.3;
    controls.enableRotate = false;
    controls.enablePan = false;
    //resize event
    window.addEventListener('resize', onWindowResize, false);
    //check if theres enough data
    if(count>0){
        tile = new AnimatedTile();
    }    
    //preloading last element in case the user navigate backwards first
    preloadImg(count-1);
    updateTiles();
    animate();
    
};


////////////////////////////////////////////////////////////////////////////////////
// UPDATING DATA
////////////////////////////////////////////////////////////////////////////////////

function updateTiles(){
    //updating the color and texture of the map
    tile.update(index);
    //updating text for place and magnitude
    var place = data[index].properties.place;
    var magnitude = data[index].properties.mag;
    $place.html(place);
    $magnitude.html(magnitude.toFixed(1) ); 
    //setting how many images will be preloaded to avoid having a blank texture
    var preloadingExtension = 5;
    //checking the index and determining which image urls will be preloaded  
    for(var i = index; i < index+preloadingExtension; i++){
       if(i<count-1){
            preloadImg(i)
       }   
    } 
    if(index>(count-index-5)){
        for(var j = index; j > index-preloadingExtension; j--){
            preloadImg(j)
        }
    }   
}

// getting image url from mapbox api based on earthquake data

function getImageTile (latitude,longitude,magnitude){
    var token = "pk.eyJ1Ijoia2lsbGluZ3pvZSIsImEiOiJjaXluM3kzcWYwMDRwMnhvdDB1bDBnZGttIn0.8HDTFf3d18mUecn5zGYNMg"
    var imageStyle =  "mapbox.streets";
    var mappedZoom= magnitude.map(0, 9, 15, 5); // 0
    var size = 1024;
    var imgUrl = "https://api.mapbox.com/v4/"+imageStyle+"/"+latitude+","+longitude+","+mappedZoom+"/"+size+"x"+size+".png?access_token="+token;
    return imgUrl;      
}



////////////////////////////////////////////////////////////////////////////////////
// PRELOADING IMAGES
////////////////////////////////////////////////////////////////////////////////////

//array of preloaded urls
var images = new Array()

//function that populate the image array
function preloadImg(i){
    if(images[i] == undefined){
        var latitude = data[i].geometry.coordinates[0];
        var longitude = data[i].geometry.coordinates[1];
        var magnitude = data[i].properties.mag;
        var imgUrl = getImageTile(latitude,longitude,magnitude);      
        images[i] = new Image();
        images[i].src = imgUrl;
    }
}


////////////////////////////////////////////////////////////////////////////////////
// NAVIGATION ACTIONS
////////////////////////////////////////////////////////////////////////////////////

var disableButtons = false;

function prevTile(){
    //disabling buttons during tile animation
    if(disableButtons) {
        return false;
    }
    //decrementing index
    index--;  
    if (index< 0){
        index = count-1;
    }
    animateUpdate();  
    tweenTile(-1);
}

function nextTile(){
    //disabling buttons during tile animation
    if(disableButtons) {
        return false;
    }
    index++;  
    if(index > count-1){
        index = 0;
    }
    animateUpdate();
    tweenTile(1);
}


////////////////////////////////////////////////////////////////////////////////////
// ANIMATIONS
////////////////////////////////////////////////////////////////////////////////////


//delaying the texture update and the place fade in
function animateUpdate(){
    setTimeout(updateTiles,300);
    setTimeout(function(){$place.fadeIn(150)},300);
}


//tweening function 
function tweenTile(direction){
    $place.fadeOut(150);
    disableButtons = true;
    new TWEEN.Tween( camera.rotation ).to( {y: rotation +2 * Math.PI * direction }, 800 )
             .easing( TWEEN.Easing.Cubic.InOut).start()
             .onComplete( function () {
                camera.rotation.y = rotation;
                disableButtons = false;            
             });
}


////////////////////////////////////////////////////////////////////////////////////
// TILE OBJECT (if we need to add multiple tiles in the future)
////////////////////////////////////////////////////////////////////////////////////

function AnimatedTile() {
    // vars
    var _this = this;
    this.texture = undefined;
    this.material= undefined;
    this.materialWireframe = undefined;
    this.geometryWireframe = undefined;
    this.wireframe = undefined;
    this.latitude= undefined;
    this.longitude= undefined;
    this.magnitude = 0;
    this.zoom= undefined;
    this.mesh = undefined;
    this.speed = 300;
    
    // updating texture
    this.updateTexture = function(imgUrl) {    
        this.texture = THREE.ImageUtils.loadTexture(imgUrl);
        this.texture.anisotropy = maxAnisotropy;
        this.texture.wrapS = THREE.RepeatWrapping; 
        this.texture.wrapT = THREE.RepeatWrapping;
        this.mesh.material.map = this.texture;
        this.mesh.material.needsUpdate = true;
    };
    
    //initializing meshes and materials
    this.init = function(){
        this.material = new THREE.MeshPhongMaterial({
             color: 0xffffff
            , shading: THREE.FlatShading
            , polygonOffset: true
            , polygonOffsetFactor: 1
            , polygonOffsetUnits: 1
        });
        var planeGeometry = new THREE.PlaneGeometry(55, 55, 100, 100);
        this.mesh = new THREE.Mesh(planeGeometry, this.material);
        this.geometryWireframe = new THREE.WireframeGeometry(this.mesh.geometry); // or WireframeGeometry
        this.materialWireframe = new THREE.LineBasicMaterial({color: 0xff0000, linewidth: 1, opacity: 0.5, transparent:true});
        this.wireframe =  new THREE.LineSegments(this.geometryWireframe, this.materialWireframe);
        this.mesh.add( this.wireframe);

        this.mesh.material.side = THREE.DoubleSide;
        this.mesh.rotation.x = -0.5 * Math.PI;
        this.mesh.rotation.z -= 10;
        this.mesh.position.set(0, 0, 0);
        scene.add(this.mesh);
        
    }
    
    //updating data visualization
    
    this.update = function(index){
        var value = data[index];
        var place = value.properties.place;
        this.magnitude = value.properties.mag;
        this.latitude = value.geometry.coordinates[0];
        this.longitude = value.geometry.coordinates[1];
        var depth = value.geometry.coordinates[2];
        var radius =  (Math.exp(this.magnitude/1.01-0.13))*1000;
        var color = "";
        if(this.magnitude > 8){
            color = 'ff0000';
            this.speed = 300;
        } else if(this.magnitude > 7){
            color = 'ffa500';
            this.speed = 600;
        } else if(this.magnitude > 6){
            color = 'FFFF00';
            this.speed = 1200;
        }
        else if(this.magnitude > 5){
            color = '00FF00';
            this.speed = 2400;
        }
        else if(this.magnitude > 4){
            color = '00FFFF';
            this.speed = 4800;
        }
        else {
            color = 'ffffff';
            this.speed = 2000;
        }
        
        $magnitude.css('color','#'+color);
        
        this.materialWireframe.color.setHex('0x'+color);
        
        
        var imgUrl = getImageTile(this.latitude,this.longitude,this.magnitude);
        this.updateTexture(imgUrl);
       
    }
    
    // animating wireframe
    
    this.animate = function(){
        var ts = Date.now();
        var center = new THREE.Vector2(0, 0);
        var vLength = this.mesh.geometry.vertices.length;
        for (var i = 0; i < vLength; i++) {
            var v = this.mesh.geometry.vertices[i];
            var dist = new THREE.Vector2(v.x, v.y).sub(center);
            //TODO: fine tunes values to better reflect data
            var mappedSize= this.magnitude.map(0, 9, 1, 9); // 0
            var mappedSpeed= this.magnitude.map(0, 9, 5000, 300); // 0
            var mappedMagnitude= this.magnitude.map(0, 9, 0.1, 3); // 0
            v.z = Math.sin(dist.length() / -mappedSize + (ts / mappedSpeed)) * (mappedMagnitude);
        }
        this.mesh.geometry.verticesNeedUpdate = true;
    }
    
   
    this.init();
}


////////////////////////////////////////////////////////////////////////////////////
// RENDER CYCLE
////////////////////////////////////////////////////////////////////////////////////

function animate(){
    window.requestAnimationFrame(animate);
    tile.animate();
    //camera.rotation.y += .1;
    TWEEN.update();
    renderer.render(scene, camera);
}


////////////////////////////////////////////////////////////////////////////////////
// EVENTS
////////////////////////////////////////////////////////////////////////////////////

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

////////////////////////////////////////////////////////////////////////////////////
// UTILITIES
////////////////////////////////////////////////////////////////////////////////////

Number.prototype.map = function (in_min, in_max, out_min, out_max) {
  return (this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

